﻿using AutoMapper;
using Dto;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiddleWare
{
    public class AutoMapperConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(
                config => {
                    #region mapping entities

                    config.CreateMap<Countries, CountriesDto>().ReverseMap();
                    config.CreateMap<Customers, CustomersDto>().ReverseMap();
                    config.CreateMap<Services, ServicesDto>().ReverseMap();
                    config.CreateMap<ServicesByCountry, ServicesByCountryDto>().ReverseMap();

                    #endregion
                });
        }
    }
}
