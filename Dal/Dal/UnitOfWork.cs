﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Dal
{
    public class UnitOfWork<TEntity> : IDisposable where TEntity : class
    {
        private DBTekusEntities context;
        private DbContextTransaction Transaction;

        public UnitOfWork()
        {
            context = new DBTekusEntities();
        }

        private GenericRepository<TEntity> Repository;

        public GenericRepository<TEntity> getRepository
        {            
            get
            {
                return this.Repository
                     ?? (this.Repository =
                         new GenericRepository<TEntity>(context));
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }

        public virtual void BeginTransaction()
        {
            Transaction = context.Database.BeginTransaction();
        }

        public virtual void EndTransaction()
        {
            Transaction.Commit();
        }

        public void RollBack()
        {
            Transaction.Rollback();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
