USE [DBTekus]
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (1, N'123', N'tekus', N'tekus@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (2, N'456', N'cognox', N'cognox@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (3, N'789', N'doctus', N'doctus@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (4, N'147', N'seiba', N'seba@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (5, N'1234', N'WorkUniversity', N'info@workuniversity.co')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (6, N'56789', N'Bancolombia', N'bancolombia@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (7, N'12345', N'davivienda', N'davivienda@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (8, N'1', N'Cliente 1', N'clinete1@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (9, N'2', N'cliente 2', N'cliente2@correo.com')
INSERT [dbo].[Customers] ([CustomerId], [NIT], [Name], [Email]) VALUES (10, N'3', N'cliente 3', N'cliente3@correo.com')

SET IDENTITY_INSERT [dbo].[Customers] OFF

SET IDENTITY_INSERT [dbo].[Services] ON 

INSERT [dbo].[Services] ([ServiceId], [CustomerId], [Name], [PriceHour]) VALUES (0, 1, N'servicio 1', 5000.0000)
INSERT [dbo].[Services] ([ServiceId], [CustomerId], [Name], [PriceHour]) VALUES (1, 1, N'servicio 2', 4900.0000)
INSERT [dbo].[Services] ([ServiceId], [CustomerId], [Name], [PriceHour]) VALUES (2, 1, N'servicio 3', 8900.0000)
INSERT [dbo].[Services] ([ServiceId], [CustomerId], [Name], [PriceHour]) VALUES (3, 5, N'WorkPrimum', 1000.0000)
INSERT [dbo].[Services] ([ServiceId], [CustomerId], [Name], [PriceHour]) VALUES (4, 5, N'WorkBasic', 0.0000)
INSERT [dbo].[Services] ([ServiceId], [CustomerId], [Name], [PriceHour]) VALUES (5, 6, N'tarjeta de credito', 30000.0000)

SET IDENTITY_INSERT [dbo].[Services] OFF

SET IDENTITY_INSERT [dbo].[Countries] ON 

INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (1, N'colombia')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (2, N'España')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (3, N'Mexico')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (4, N'Chile')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (5, N'Alemania')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (6, N'Argentina')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (7, N'Luxenbur')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (8, N'Estados unidos')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (9, N'Brasil')
INSERT [dbo].[Countries] ([CountryId], [Name]) VALUES (10, N'Francia')

SET IDENTITY_INSERT [dbo].[Countries] OFF

SET IDENTITY_INSERT [dbo].[ServicesByCountry] ON 

INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (0, 0, 1)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (1, 0, 2)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (2, 0, 3)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (3, 0, 4)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (4, 1, 1)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (5, 1, 2)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (6, 1, 3)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (7, 1, 4)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (8, 1, 5)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (9, 1, 6)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (10, 2, 1)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (11, 3, 1)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (12, 4, 1)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (13, 5, 1)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (14, 5, 2)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (15, 5, 3)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (16, 5, 4)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (17, 5, 5)
INSERT [dbo].[ServicesByCountry] ([ServicesByCountryId], [ServiceId], [CountryId]) VALUES (18, 5, 6)

SET IDENTITY_INSERT [dbo].[ServicesByCountry] OFF

