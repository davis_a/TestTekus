﻿//        Uso: FormattedUrl = Url a hacer peticion,
//        MethodType : GET / POST / DELETE / PUT
//        DataType : application/json; charset=utf-8 application/xml; charset=utf-8
//        Cache : Activar cache Boolean
//        InData : Si vamos a enviar datos (Ver ejemplo)
//
//        Ejemplo : 
//        var URL = FormatString(UrlsPlatforms.GetById, Language.Culture, id);
//        var inData = {
//              PlatformId: 0,
//              Name: $("#Name").val(),
//              Description: $("#description").val(),
//              UserId_Update: 0}; 
//        ó var inData = null;
//        var data = Factory.HttpRequest(URL, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null);


var FactoryMethods = { GET: "GET", POST: "POST", PUT: "PUT", HEAD: "HEAD", DELETE: "DELETE", PATCH: "PATCH" };
var FactoryDataTypes = { JSON: "json", XML: "xml" }
var FactoryContentTypes = { JSON: "application/json; charset=utf-8" }

var Factory = {
    Async: true,
    HttpRequest: function (FormattedUrl, MethodType, ContentType,
                            DataType, Cache, InData, SuccesscallBack, ErrorCallBack, DoneCallback) {

        var result;

        if (InData != null) {
            $.ajax({
                url: FormattedUrl,
                contentType: ContentType,
                method: MethodType,
                dataType: DataType,
                cache: Cache,
                async: Factory.Async,
                data: JSON.stringify(InData),
                success: SuccesscallBack,
                error: ErrorCallBack
            }).done(DoneCallback);
        }
        else {
            $.ajax({
                url: FormattedUrl,
                contentType: "application/json; charset=utf-8",
                method: MethodType,
                dataType: DataType,
                cache: Cache,
                async: Factory.Async,
                success: SuccesscallBack,
                error: ErrorCallBack
            }).done(DoneCallback);
        }

        return result;
    }
}