﻿
var StorageJs = {
    setStorage: function (NameStorage, object)
    {
        localStorage.setItem(NameStorage, JSON.stringify(object));
    },
    getStorage: function (NameStorage)
    {
        const data = JSON.parse(localStorage.getItem(NameStorage));
        return data;
    },
    defaultStorage: function ()
    {
        StorageJs.setStorage("Customers", []);
        StorageJs.setStorage("Countries", []);
        StorageJs.setStorage("Services", []);
    }
};