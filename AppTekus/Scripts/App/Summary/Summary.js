﻿
var Events = {
    Init: function () {
        Events.LoadCustomers();
        Events.LoadServices();
    },
    LoadCustomers: function () {
        var SuccessCallback = function (data) {
            if (data.Response.ResponseState) {
                let countCustomers = data.Response.ResponseObject.length;
                $("#lblCustomer").text(countCustomers + " customers have been created");
            } else {
                alert(data.Response.ResponseMessage);
            }
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            //Configurar la Url
            let Url = "http://localhost:62566/Api/Customers/GetCustomers";
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Customers = StorageJs.getStorage("Customers");
            $("#lblCustomer").text(Customers.length + " customers have been created");
        }
    },
    LoadServices: function () {
        var SuccessCallback = function (data) {
            if (data.Response.ResponseState) {
                let countServices = data.Response.ResponseObject.length;
                $("#lblServices").text(countServices + " services have been created");
                Events.GetCountries(data.Response.ResponseObject);
            } else {
                alert(data.Response.ResponseMessage);
            }
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            //Configurar la Url
            let Url = "http://localhost:62566/Api/Services/GetServices";
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Services = StorageJs.getStorage("Services");
            $("#lblServices").text(Services.length + " services have been created");
            Events.GetCountries(Services);
        }

    },
    GetCountries: function (services) {
        $('#Countries').empty();
        var SuccessCallback = function (data) {
            if (data.Response.ResponseState) {
                let Countries = data.Response.ResponseObject;
                Events.GenerateListCountries(Countries, services);
            } else {
                alert(data.Response.ResponseMessage);
            }
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            //Configurar la Url
            let Url = "http://localhost:62566/Api/Countries/GetCountries";
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Countries = StorageJs.getStorage("Countries");
            Events.GenerateListCountries(Countries, services);
        }
    },
    ShowCountries: function (data) {
        $('#tblCountries').DataTable().clear();
        $('#tblCountries').DataTable().destroy();
        var $DataTable = $('#tblCountries').DataTable({
            scrollX: true,
            scrollCollapse: true,
            data: data,
            paging: true,
            searching: true,
            ordering: true,
            columns: [
                { "data": "Name" },
                { "data": "Services" },
            ]
        });
    },
    GenerateListCountries: function (Countries, services) {
        let CountriesServices = [];
        $.each(Countries, function (index, value) {
            let Quantity = services.filter(function (e) { return e.CountryId == value.CountryId }).length;
            let Country = {
                Name: value.Name,
                Services: Quantity,
            };
            CountriesServices.push(Country);
        });
        Events.ShowCountries(CountriesServices);
    }
};