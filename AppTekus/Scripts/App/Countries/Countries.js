﻿
var Events = {
    Action: "",
    Init: function () {
        Events.LoadData();
    },
    LoadData: function () {
        var SuccessCallback = function (data) {
            if (data.Response.ResponseState) {
                Events.ShowCountries(data.Response.ResponseObject);
            } else {
                alert(data.Response.ResponseMessage);
            }
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            //Configurar la Url
            let Url = "http://localhost:62566/Api/Countries/GetCountries";
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Countries = StorageJs.getStorage("Countries");
            Events.ShowCountries(Countries);
        }
    },
    ShowCountries: function (data) {
        $('#tblCountries').DataTable().clear();
        $('#tblCountries').DataTable().destroy();
        var $DataTable = $('#tblCountries').DataTable({
            scrollY: '50vh',
            scrollX: true,
            scrollCollapse: true,
            data: data,
            paging: true,
            searching: true,
            ordering: true,
            columns: [
                { "data": "Name" },
                { "defaultContent": Events.GetButtonEdit() },
                { "defaultContent": Events.GetButtonDelete() },
            ],
        });
        Events.ActionButtons($DataTable);
    },
    ActionButtons: function () {
        $('#tblCountries').off('click', 'button');
        $('#tblCountries').on('click', 'button', function () {
            var table = $('#tblCountries').DataTable();
            var myClass = $(this).attr("class");
            var result = myClass.search("btnActionEdit");
            var data = table.row($(this).parents('tr')).data();
            if (result != -1) {
                Events.Action = "Update";
                $(".modal-title").text("Edit");
                $("#modalEdit").modal("show");
                $("#Name").val(data.Name);
                $("#btnSave").attr("data", data.CountryId);
            }
            result = myClass.search("btnActionDelete");
            if (result != -1) {
                $("#btnDelete").attr("data", data.CountryId);
                $("#modalDelete").modal("show");
            }
        });
    },
    Delete: function () {
        var SuccessCallback = function (data) {
            $("#modalDelete").modal("hide");
            Events.LoadData();
            alert(data.Response.ResponseMessage);
        };

        let Id = $("#btnDelete").attr("data");

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            //Configurar la Url
            let Url = "http://localhost:62566/Api/Countries/DeleteCountries?CountryId=" + Id;
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Index = -1;
            let Countries = StorageJs.getStorage("Countries");
            $.each(Countries, function (indx, value) { if (value.CountryId == Id) { Index = indx; } });
            Countries.splice(Index, 1);
            StorageJs.setStorage("Countries", Countries);
            alert("Country successfully deleted");
            $("#modalDelete").modal("hide");
            Events.LoadData();
        }
    },
    New: function () {
        Events.Action = "New";
        $(".modal-title").text("New");
        $("#modalEdit").modal("show");
        $("#Name").val("");
    },
    Saved: function () {

        if (!Events.ValidateDataCountry()) {
            return;
        }

        var SuccessCallback = function (data) {
            $("#modalEdit").modal("hide");
            Events.LoadData();
            alert(data.Response.ResponseMessage);
        };

        let element = {
            CountryId: 0,
            Name: $("#Name").val(),
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            let ActionMethod = "CreateCountries";

            if (Events.Action == "Update") {
                element.CountryId = $("#btnSave").attr("data");
                ActionMethod = "UpdateCountries";
            }

            let Url = "http://localhost:62566/Api/Countries/" + ActionMethod;
            Factory.HttpRequest(Url, FactoryMethods.POST, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, element, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Countries = StorageJs.getStorage("Countries");
            if (Events.Action == "Update") {
                let id = $("#btnSave").attr("data");
                $.each(Customers, function (index, value) {
                    if (value.CountryId == id) {
                        value.Name = element.Name;
                    }
                });
                alert("successfully edited country");
            }
            else {
                let id = Countries.length + 1;
                element.CountryId = id;
                Countries.push(element);
                alert("successfully created country");
            }
            StorageJs.setStorage("Countries", Countries);
            $("#modalEdit").modal("hide");
            Events.LoadData();
        }
    },
    ValidateDataCountry: function () {
        let Val = true;
        if ($("#Name").val().trim() == "") {
            Val = false;
            alert("The field name is required");
        }

        return Val;
    },
    GetButtonEdit: function () {
        return '<button style="background: transparent; color: black;" class="btn btn-primary btnActionEdit">Edit</button>'
    },
    GetButtonDelete: function () {
        return '<button style="background: transparent; color: black;" class="btn btn-danger btnActionDelete">Delete</button>'
    },
    ErrorCallback: function (jqXHR, textStatus, errorThrown) {
        alert(errorThrown);
    },
};