﻿
var Events = {
    Action: "",
    Init: function ()
    {
        if (StorageJs.getStorage("TypeStorage") != null && StorageJs.getStorage("TypeStorage") == "Local") {
            $("#Storage").attr("checked", "checked");
        }
        Events.TypeStorage();
        Events.LoadData();
        Events.GetCountries();
        
        $('#modalServices').on('shown.bs.modal', function (e) {
            $('#tblServices').DataTable().columns.adjust().draw();
        })
        $("#Storage").change(function (e) {
            Events.TypeStorage();
            Events.LoadData();
            Events.GetCountries();
        });
    },
    TypeStorage: function ()
    {
        if (StorageJs.getStorage("TypeStorage") == null) {
            StorageJs.defaultStorage();
        }
        let value = $("#Storage").is(":checked");
        if (value) {
            StorageJs.setStorage("TypeStorage", "Local");
        }
        else {
            StorageJs.setStorage("TypeStorage", "Server");
        }
    },
    GetCountries: function()
    {
        $('#Countries').empty();
        var SuccessCallback = function (data) {
            if (data.Response.ResponseState) {
                let Countries = data.Response.ResponseObject;
                Events.FilterCountries(Countries);
            } else {
                alert(data.Response.ResponseMessage);
            }
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            //Configurar la Url
            let Url = "http://localhost:62566/Api/Countries/GetCountries";
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Countries = StorageJs.getStorage("Countries");
            Events.FilterCountries(Countries);
        }
    },
    FilterCountries: function (Countries) {
        $('#Countries').empty();
        $.each(Countries, function (key, objCountry) {
            $('#Countries')
                .append($("<option></option>")
                    .attr("value", objCountry.CountryId)
                    .text(objCountry.Name));
        });
        $('#Countries').selectpicker('refresh');
    },
    LoadData: function () {
        var SuccessCallback = function (data) {
            if (data.Response.ResponseState) {
                Events.ShowCustomers(data.Response.ResponseObject);
            } else {
                alert(data.Response.ResponseMessage);
            }
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            //Configurar la Url
            let Url = "http://localhost:62566/Api/Customers/GetCustomers";
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Customers = StorageJs.getStorage("Customers");
            Events.ShowCustomers(Customers);
        }        
    },
    ShowCustomers: function (data) {
        $('#tblCustomers').DataTable().clear();
        $('#tblCustomers').DataTable().destroy();
        var $DataTable = $('#tblCustomers').DataTable({
            scrollY: '50vh',
            scrollX: true,
            scrollCollapse: true,
            data: data,
            paging: true,
            searching: true,
            ordering: true,
            columns: [
                { "data": "NIT" },
                { "data": "Name" },
                { "data": "Email" },
                { "defaultContent": Events.GetButtonEdit() },
                { "defaultContent": Events.GetButtonDelete() },
                { "defaultContent": Events.GetButtonServices() },
            ],
        });
        Events.ActionButtons();
    },
    LoadServices: function () {
        var SuccessCallback = function (data)
        {            
            if (data.Response.ResponseState) {
                Events.ShowServices(data.Response.ResponseObject);
            } else {
                alert(data.Response.ResponseMessage);
            }            
        };

        if ($("#btnSavedService").attr("data") != null) {
            if (StorageJs.getStorage("TypeStorage") == "Server") {
                //Configurar la Url
                let Url = "http://localhost:62566/Api/Services/GetServicesByCustomer?CustomerId=" + $("#btnSavedService").attr("data");
                Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
            }
            else if (StorageJs.getStorage("TypeStorage") == "Local") {
                let Services = StorageJs.getStorage("Services");
                Events.ShowServices(Services);
            }
        }
    },
    ShowServices: function (data)
    {
        $('#tblServices').DataTable().clear();
        $('#tblServices').DataTable().destroy();
        var $DataTable = $('#tblServices').DataTable({
            scrollX: true,
            scrollCollapse: true,
            data: data,
            paging: true,
            searching: true,
            ordering: true,
            columns: [
                { "data": "Services.Name" },
                { "data": "Services.PriceHour" },
                { "data": "Countries.Name" },
                { "defaultContent": Events.GetButtonDeleteService() },
            ]
        });
        Events.ActionButtonsServices();
    },
    ActionButtons: function () {
        $('#tblCustomers').off('click', 'button');
        $('#tblCustomers').on('click', 'button', function () {
            var table = $('#tblCustomers').DataTable();
            var myClass = $(this).attr("class");
            var result = myClass.search("btnActionEdit");
            var data = table.row($(this).parents('tr')).data();
            if (result != -1) {
                Events.Action = "Update";
                $(".modal-title").text("Edit");
                $("#modalEdit").modal("show");
                $("#NIT").val(data.NIT);
                $("#Name").val(data.Name);
                $("#Email").val(data.Email);
                $("#btnSave").attr("data",data.CustomerId);
            } 
            result = myClass.search("btnActionDelete");
            if (result != -1) {
                $("#btnDelete").attr("data", data.CustomerId);
                $("#btnDelete").attr("Origin", "Customer");
                $("#modalDelete").modal("show");
            } 
            result = myClass.search("btnActionServices");
            if (result != -1) {
                $("#btnSavedService").attr("data", data.CustomerId);
                Events.LoadServices();
                $("#modalServices").modal("show");
            }
        });
    },
    ActionButtonsServices: function () {
        $('#tblServices').off('click', 'span');
        $('#tblServices').on('click', 'span', function () {
            var table = $('#tblServices').DataTable();
            var myClass = $(this).attr("class");
            var result = myClass.search("btnActionServicesDelete");
            var data = table.row($(this).parents('tr')).data();
            if (result != -1) {
                $("#btnDelete").attr("data", data.ServicesByCountryId);
                $("#btnDelete").attr("Origin", "Service");
                $("#modalDelete").modal("show");
            }
        });
    },
    ActionDeleteAll: function () {
        $("#btnDelete").attr("Origin", "DeleteAll");
        $("#modalDelete").modal("show");
    },
    Delete: function()
    {
        var SuccessCallback = function (data) {
            $("#modalDelete").modal("hide");
            Events.LoadData();
            Events.GetCountries();
            Events.LoadServices();
            alert(data.Response.ResponseMessage);
        };

        let UrlAction = "";
        let Origin = $("#btnDelete").attr("Origin");
        let Id = $("#btnDelete").attr("data");

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            if (Origin == "Customer") {
                UrlAction = "/Customers/DeleteCustomers?CustomerId=" + Id;
            }
            else if (Origin == "Service") {
                UrlAction = "/Services/DeleteService?ServiceId=" + Id;
            }
            else if (Origin == "DeleteAll") {
                UrlAction = "/Common/DeteleAll";
            }

            let Url = "http://localhost:62566/Api" + UrlAction;
            Factory.HttpRequest(Url, FactoryMethods.GET, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, null, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            if (Origin == "Customer") {
                let Index = -1;
                let Customers = StorageJs.getStorage("Customers");
                $.each(Customers, function (indx, value) { if (value.CustomerId == Id) { Index = indx; } });
                Customers.splice(Index, 1);
                StorageJs.setStorage("Customers", Customers);
                alert("Customer successfully deleted");
            }
            else if (Origin == "Service") {
                let Index = -1;
                let Services = StorageJs.getStorage("Services");
                $.each(Services, function (indx, value) { if (value.ServicesByCountryId == Id) { Index = indx; } });
                Services.splice(Index, 1);
                StorageJs.setStorage("Services", Services);
                alert("Service successfully deleted");
            }
            else if (Origin == "DeleteAll") {
                StorageJs.defaultStorage();
                alert("Successful massive deletion");
            }
            $("#modalDelete").modal("hide");
            Events.LoadData();
            Events.GetCountries();
            Events.LoadServices();
        }
    },
    New: function () {
        Events.Action = "New";
        $(".modal-title").text("New");
        $("#modalEdit").modal("show");
        $("#NIT").val("");
        $("#Name").val("");
        $("#Email").val("");
    },
    Saved: function () {

        if (!Events.ValidateDataCustomer()) {
            return;
        }

        var SuccessCallback = function (data) {
            $("#modalEdit").modal("hide");
            Events.LoadData();
            alert(data.Response.ResponseMessage);
        };

        let element = {
            CustomerId: 0,
            NIT: $("#NIT").val(),
            Name: $("#Name").val(),
            Email: $("#Email").val(),
        };

        if (StorageJs.getStorage("TypeStorage") == "Server")
        {
            let ActionMethod = "CreateCustomers";

            if (Events.Action == "Update") {
                element.CustomerId = $("#btnSave").attr("data");
                ActionMethod = "UpdateCustomers";
            }
            let Url = "http://localhost:62566/Api/Customers/" + ActionMethod;
            Factory.HttpRequest(Url, FactoryMethods.POST, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, element, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let Customers = StorageJs.getStorage("Customers");
            if (Events.Action == "Update") {
                let id = $("#btnSave").attr("data");
                $.each(Customers, function (index, value) {
                    if (value.CustomerId == id) {
                        value.NIT = element.NIT;
                        value.Name = element.Name;
                        value.Email = element.Email;
                    }
                });
                alert("successfully edited customer");
            }
            else {
                let id = Customers.length + 1;
                element.CustomerId = id;
                Customers.push(element);
                alert("successfully created customer");
            }
            StorageJs.setStorage("Customers", Customers);
            $("#modalEdit").modal("hide");
            Events.LoadData();
        }
    },
    SavedService: function () {

        if (!Events.ValidateDataService()) {
            return;
        }

        var SuccessCallback = function (data) {
            Events.LoadServices();
            alert(data.Response.ResponseMessage);
        };

        let element = {
            ServiceId: 0,
            CustomerId: $("#btnSavedService").attr("data"),
            Name: $("#NameService").val(),
            PriceHour: $("#PriceService").val(),
        };

        if (StorageJs.getStorage("TypeStorage") == "Server") {
            let strCountries = "";

            if ($("#Countries").val() != null) {
                $.each($("#Countries").val(), function (index, value) {
                    strCountries += value;
                    if (index + 1 < $("#Countries").val().length) {
                        strCountries += ",";
                    }
                });
            }

            let Url = "http://localhost:62566/Api/Services/CreateService?strCountries=" + strCountries;
            Factory.HttpRequest(Url, FactoryMethods.POST, FactoryContentTypes.JSON, FactoryDataTypes.JSON, false, element, SuccessCallback, Events.ErrorCallback);
        }
        else if (StorageJs.getStorage("TypeStorage") == "Local") {
            let lstServices = StorageJs.getStorage("Services");
            let Countries = StorageJs.getStorage("Countries");
            let id = lstServices.length;

            if ($("#Countries").val() != null) {
                $.each($("#Countries").val(), function (index, value) {
                    id += 1;
                    let Service = {
                        ServicesByCountryId: id,
                        ServiceId: 0,
                        CountryId: parseInt(value),
                        Countries: Countries.filter(function (e) { return e.CountryId == parseInt(value) })[0],
                        Services: element
                    };
                    lstServices.push(Service);
                });
            }
            StorageJs.setStorage("Services", lstServices);
            alert("Service created successfully");
            Events.LoadServices();
        }
    },
    ValidateDataCustomer: function () {
        let Val = true;
        if ($("#NIT").val().trim() == "" || $("#Name").val().trim() == "" || $("#Email").val().trim() == "") {
            Val = false;
            alert("All fields are required");
        }

        emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        if ($("#Email").val().trim() != "" && !emailRegex.test($("#Email").val())) {
            Val = false;
            alert("Enter a valid email address");
        }

        return Val;
    },
    ValidateDataService: function () {
        let Val = true;
        if ($("#NameService").val().trim() == "" || $("#PriceService").val().trim() == "" || $("#Countries").val() == null) {
            Val = false;
            alert("All fields are required");
        }

        return Val;
    },
    GetButtonEdit: function () {
        return '<button style="background: transparent; color: black;" class="btn btn-primary btnActionEdit">Edit</button>'
    },
    GetButtonDelete: function () {
        return '<button style="background: transparent; color: black;" class="btn btn-danger btnActionDelete">Delete</button>'
    },
    GetButtonServices: function () {
        return '<button style="background: transparent; color: black;" class="btn btn-danger btnActionServices">Servicios</button>'
    },
    GetButtonDeleteService: function () {
        return '<span class="glyphicon glyphicon-remove btnActionServicesDelete"></span>'
    },
    ErrorCallback: function (jqXHR, textStatus, errorThrown) {
        alert(errorThrown);
    },
};