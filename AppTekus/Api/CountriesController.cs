﻿using Business.Dao;
using Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppTekus.Api
{
    public class CountriesController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetCountries()
        {
            CountriesDao _CountriesDao = new CountriesDao();

            ResponseDto objResponse = _CountriesDao.GetCountries();

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpPost]
        public HttpResponseMessage CreateCountries(CountriesDto objCountries)
        {
            CountriesDao _CountriesDao = new CountriesDao();

            ResponseDto objResponse = _CountriesDao.CreateCountries(objCountries);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpPost]
        public HttpResponseMessage UpdateCountries(CountriesDto objCountry)
        {
            CountriesDao _CountriesDao = new CountriesDao();

            ResponseDto objResponse = _CountriesDao.UpdateCountries(objCountry);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpGet]
        public HttpResponseMessage DeleteCountries(int CountryId)
        {
            CountriesDao _CountriesDao = new CountriesDao();

            ResponseDto objResponse = _CountriesDao.DeleteCountries(CountryId);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }
    }
}
