﻿using Business.Dao;
using Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppTekus.Api
{
    public class CommonController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage DeteleAll()
        {
            CommonDao _CommonDao = new CommonDao();

            ResponseDto objResponse = _CommonDao.DeleteAll();

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }
    }
}
