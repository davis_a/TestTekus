﻿using Business.Dao;
using Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppTekus.Api
{
    public class ServicesController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetServices()
        {
            ServicesDao _ServicesDao = new ServicesDao();

            ResponseDto objResponse = _ServicesDao.GetServices();

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpGet]
        public HttpResponseMessage GetServicesByCustomer(int CustomerId)
        {
            ServicesDao _ServicesDao = new ServicesDao();

            ResponseDto objResponse = _ServicesDao.GetServicesByCustomer(CustomerId);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpPost]
        public HttpResponseMessage CreateService(string strCountries,[FromBody] ServicesDto objServiceDto)
        {
            ServicesDao _ServicesDao = new ServicesDao();

            ResponseDto objResponse = _ServicesDao.CreateService(objServiceDto, strCountries);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpGet]
        public HttpResponseMessage DeleteService(int ServiceId)
        {
            ServicesDao _ServicesDao = new ServicesDao();

            ResponseDto objResponse = _ServicesDao.DeleteService(ServiceId);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

    }
}
