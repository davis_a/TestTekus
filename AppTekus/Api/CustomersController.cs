﻿using Business.Dao;
using Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppTekus.Api
{
    public class CustomersController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetCustomers()
        {
            CustomersDao _CustomersDao = new CustomersDao();

            ResponseDto objResponse = _CustomersDao.GetCustomers();

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpPost]
        public HttpResponseMessage CreateCustomers(CustomersDto objCustomer)
        {
            CustomersDao _CustomersDao = new CustomersDao();

            ResponseDto objResponse = _CustomersDao.CreateCustomers(objCustomer);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpPost]
        public HttpResponseMessage UpdateCustomers(CustomersDto objCustomer)
        {
            CustomersDao _CustomersDao = new CustomersDao();

            ResponseDto objResponse = _CustomersDao.UpdateCustomers(objCustomer);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }

        [HttpGet]
        public HttpResponseMessage DeleteCustomers(int CustomerId)
        {
            CustomersDao _CustomersDao = new CustomersDao();

            ResponseDto objResponse = _CustomersDao.DeleteCustomers(CustomerId);

            return Request.CreateResponse(objResponse.ResponseState ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, new { Response = objResponse });
        }
    }
}
