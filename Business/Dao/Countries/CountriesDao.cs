﻿using AutoMapper;
using Dal;
using Dto;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Dao
{
    public class CountriesDao
    {
        private readonly UnitOfWork<Countries> _Countries;

        public CountriesDao()
        {
            _Countries = new UnitOfWork<Countries>();
        }

        public ResponseDto GetCountries()
        {
            ResponseDto objResponse;
            try
            {
                List<Countries> lstCountries = _Countries.getRepository.Get().ToList();
                List<CountriesDto> lstCountriesDto = Mapper.Map<List<Countries>, List<CountriesDto>>(lstCountries);

                objResponse = new ResponseDto
                {
                    ResponseObject = lstCountriesDto,
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }

        public ResponseDto CreateCountries(CountriesDto objCountriesDto)
        {
            ResponseDto objResponse;
            try
            {
                Countries objCountries = Mapper.Map<CountriesDto, Countries>(objCountriesDto);
                _Countries.getRepository.Create(objCountries);
                _Countries.Save();

                objResponse = new ResponseDto
                {
                    ResponseMessage = "successfully created country",
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }

        public ResponseDto UpdateCountries(CountriesDto objCountriesDto)
        {
            ResponseDto objResponse;
            try
            {
                Countries objCountries = new Countries
                {
                    CountryId = objCountriesDto.CountryId,
                    Name = objCountriesDto.Name,
                };
                _Countries.getRepository.Update(objCountries);
                _Countries.Save();

                objResponse = new ResponseDto
                {
                    ResponseMessage = "successfully edited country",
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }

        public ResponseDto DeleteCountries(int CountriesId)
        {
            ResponseDto objResponse;
            try
            {
                _Countries.getRepository.Delete(CountriesId);
                _Countries.Save();

                objResponse = new ResponseDto
                {
                    ResponseMessage = "Country successfully deleted",
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }
    }
}
