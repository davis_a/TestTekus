﻿using AutoMapper;
using Dal;
using Dto;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Dao
{
    public class ServicesDao
    {
        private readonly UnitOfWork<Services> _Services;
        private readonly UnitOfWork<ServicesByCountry> _ServicesByCountry;

        public ServicesDao()
        {
            _Services = new UnitOfWork<Services>();
            _ServicesByCountry = new UnitOfWork<ServicesByCountry>();
        }

        public ResponseDto GetServices()
        {
            ResponseDto Response;
            try
            {
                List<ServicesByCountry> lstServicesByCountry = _ServicesByCountry.getRepository.Get().ToList();
                List<ServicesByCountryDto> lstServicesByCountryDto = Mapper.Map<List<ServicesByCountry>, List<ServicesByCountryDto>>(lstServicesByCountry);

                Response = new ResponseDto
                {
                    ResponseState = true,
                    ResponseType = "Success",
                    ResponseObject = lstServicesByCountryDto
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                Response = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return Response;
        }

        public ResponseDto GetServicesByCustomer(int CustomerId)
        {
            ResponseDto Response;
            try
            {
                ModelJoins _Join = new ModelJoins();
                _Join.AddJoin(nameof(ServicesByCountry.Services));
                _Join.AddJoin(nameof(ServicesByCountry.Countries));
                List<ServicesByCountry> lstServicesByCountry = _ServicesByCountry.getRepository.Get(x => x.Services.CustomerId == CustomerId, null, _Join.GetJoins()).ToList();
                List<ServicesByCountryDto> lstServicesByCountryDto = Mapper.Map<List<ServicesByCountry>, List<ServicesByCountryDto>>(lstServicesByCountry);

                Response = new ResponseDto
                {
                    ResponseState = true,
                    ResponseType = "Success",
                    ResponseObject = lstServicesByCountryDto
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                Response = new ResponseDto {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return Response;
        }

        public ResponseDto CreateService(ServicesDto objServiceDto, string strCountries)
        {
            ResponseDto Response;
            try
            {
                _Services.BeginTransaction();
                _ServicesByCountry.BeginTransaction();

                Services objService = Mapper.Map<ServicesDto, Services>(objServiceDto);
                _Services.getRepository.Create(objService);
                _Services.Save();

                List<ServicesByCountry> lstServicesByCountry = new List<ServicesByCountry>();
                List<string> lstCountries = strCountries.Split(',').ToList();
                foreach (string CountryId in lstCountries)
                {
                    lstServicesByCountry.Add(new ServicesByCountry { CountryId = Convert.ToInt32(CountryId), ServiceId = objService.ServiceId });
                }

                _ServicesByCountry.getRepository.Create(lstServicesByCountry);
                _Services.EndTransaction();
                _ServicesByCountry.Save();

                _ServicesByCountry.EndTransaction();
                
                

                Response = new ResponseDto
                {
                    ResponseState = true,
                    ResponseType = "Success",
                    ResponseMessage = "Service created successfully"
                };
            }
            catch (Exception Ex)
            {
                _ServicesByCountry.RollBack();
                _Services.RollBack();
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                Response = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return Response;
        }

        public ResponseDto DeleteService(int ServiceId)
        {
            ResponseDto Response;
            try
            {
                ServicesByCountry objServicesByCountry = _ServicesByCountry.getRepository.Get(x => x.ServicesByCountryId == ServiceId).FirstOrDefault();
                _ServicesByCountry.getRepository.Delete(objServicesByCountry);
                _ServicesByCountry.Save();

                Response = new ResponseDto
                {
                    ResponseState = true,
                    ResponseType = "Success",
                    ResponseMessage = "Service successfully deleted"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                Response = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return Response;
        }
    }
}
