﻿using Dto;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Dao
{
    public class CommonDao
    {
        private string ConnectionString;

        public CommonDao()
        {
            Connection();
        }

        public ResponseDto DeleteAll()
        {
            ResponseDto Response;
            Database objDB = new SqlDatabase(ConnectionString);

            using (DbCommand objcmd = objDB.GetStoredProcCommand("USP_DeleteAll"))
            {
                DataSet dsResponse = objDB.ExecuteDataSet(objcmd);
                if (dsResponse.Tables.Count > 0)
                {
                    Response = new ResponseDto {
                        ResponseObject = dsResponse.Tables[0],
                        ResponseState = false,
                        ResponseType = "Error",
                        ResponseMessage = "Error",
                    };
                }
                else
                {
                    Response = new ResponseDto
                    {
                        ResponseMessage = "all records were successfully deleted",
                        ResponseState = true,
                        ResponseType = "Success"
                    };
                }

                return Response;
            }
        }

        #region private Methods

        private void Connection()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["Tekus"].ToString();
        }

        #endregion
    }
}
