﻿using AutoMapper;
using Dal;
using Dto;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business.Dao
{
    public class CustomersDao
    {
        private readonly UnitOfWork<Customers> _Customers;

        public CustomersDao()
        {
            _Customers = new UnitOfWork<Customers>();
        }


        public ResponseDto GetCustomers()
        {
            ResponseDto objResponse;
            try
            {
                List<Customers> lstCustomers = _Customers.getRepository.Get().ToList();
                List<CustomersDto> lstCustomersDto = Mapper.Map<List<Customers>, List<CustomersDto>>(lstCustomers);

                objResponse = new ResponseDto {
                    ResponseObject = lstCustomersDto,
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }

        public ResponseDto CreateCustomers(CustomersDto objCustomerDto)
        {
            ResponseDto objResponse;
            try
            {
                Customers objCustomer = Mapper.Map<CustomersDto, Customers>(objCustomerDto);
                _Customers.getRepository.Create(objCustomer);
                _Customers.Save();                

                objResponse = new ResponseDto
                {
                    ResponseMessage = "successfully created customer",
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }

        public ResponseDto UpdateCustomers(CustomersDto objCustomerDto)
        {
            ResponseDto objResponse;
            try
            {
                Customers objCustomer = new Customers {
                    CustomerId = objCustomerDto.CustomerId,
                    Email = objCustomerDto.Email,
                    Name = objCustomerDto.Name,
                    NIT = objCustomerDto.NIT
                };
                _Customers.getRepository.Update(objCustomer);
                _Customers.Save();

                objResponse = new ResponseDto
                {
                    ResponseMessage = "successfully edited customer",
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }

        public ResponseDto DeleteCustomers(int CustomerId)
        {
            ResponseDto objResponse;
            try
            {
                _Customers.getRepository.Delete(CustomerId);
                _Customers.Save();

                objResponse = new ResponseDto
                {
                    ResponseMessage = "Customer successfully deleted",
                    ResponseState = true,
                    ResponseType = "Success"
                };
            }
            catch (Exception Ex)
            {
                while (Ex.InnerException != null)
                {
                    Ex = Ex.InnerException;
                }
                objResponse = new ResponseDto
                {
                    ResponseMessage = Ex.Message,
                    ResponseState = false,
                    ResponseType = "Error"
                };
            }

            return objResponse;
        }
    }
}
