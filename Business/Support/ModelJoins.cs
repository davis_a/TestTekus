﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business
{
    internal class ModelJoins
    {
        private readonly List<string> Joins;

        public ModelJoins()
        {
            Joins = new List<string>();
        }

        public void AddJoin(params string[] ParamJoins)
        {
            StringBuilder sSendJoin = new StringBuilder();

            for (int index = 0; index < ParamJoins.Length; index++)
            {
                if (index == ParamJoins.Length - 1)
                {
                    sSendJoin.Append(ParamJoins[index]);
                }
                else
                {
                    sSendJoin.Append(ParamJoins[index] + ".");
                }
            }

            Joins.Add(sSendJoin.ToString());
        }

        public string GetJoins()
        {
            return string.Join(",", Joins);
        }

        public void CleanJoins()
        {
            Joins.Clear();
        }
    }
}
