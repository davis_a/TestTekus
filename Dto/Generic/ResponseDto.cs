﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class ResponseDto
    {
        public string ResponseType { get; set; }
        public string ResponseMessage { get; set; }
        public object ResponseObject { get; set; }
        public bool ResponseState { get; set; }
    }
}
