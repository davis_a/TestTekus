﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class ServicesDto
    {
        public int ServiceId { get; set; }
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public decimal PriceHour { get; set; }
        public CustomersDto Customers { get; set; }
    }
}
