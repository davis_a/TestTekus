﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public class ServicesByCountryDto
    {
        public int ServicesByCountryId { get; set; }
        public int ServiceId { get; set; }
        public int CountryId { get; set; }

        public CountriesDto Countries { get; set; }
        public ServicesDto Services { get; set; }
    }
}
