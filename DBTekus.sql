create database DBTekus 
go
use DBTekus
go
create table Customers
(
	CustomerId int identity not null,
	NIT varchar(20) not null,
	Name varchar(50) not null,
	Email varchar(100) not null,
	primary key(CustomerId)
)
create table [Services]
(
	ServiceId int identity not null,
	CustomerId int not null,
	Name varchar(100) not null,
	PriceHour money not null,
	Primary Key(ServiceId),
	Foreign Key(CustomerId) references Customers(CustomerId)
) 
create table Countries
(
	CountryId int identity not null,
	Name varchar(50) not null,
	Primary Key(CountryId)
)
Create table ServicesByCountry
(
	ServicesByCountryId int identity not null,
	ServiceId int not null,
	CountryId int not null,
	primary key(ServicesByCountryId),
	foreign key(ServiceId) references [Services](ServiceId),
	foreign key(CountryId) references Countries(CountryId),
)
go
CREATE PROCEDURE USP_DeleteAll AS
BEGIN

BEGIN TRY
BEGIN TRANSACTION TRDelete

	delete ServicesByCountry
	DBCC CHECKIDENT (ServicesByCountry, RESEED,0)
	delete Countries
	DBCC CHECKIDENT (Countries, RESEED,0)
	delete [Services]
	DBCC CHECKIDENT ([Services], RESEED,0)
	delete Customers
	DBCC CHECKIDENT (Customers, RESEED,0)

COMMIT TRANSACTION TRDelete

END TRY

BEGIN CATCH

	SELECT ERROR_MESSAGE() AS Error

	ROLLBACK TRANSACTION TRDelete
END CATCH

end